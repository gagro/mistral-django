from django.shortcuts import render
from employees.models import Department, Employee
from employees.serializers import DepartmentSerializer, EmployeeSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
import xmltodict


def home(request):
    return render(request, "employees/index.html")

def employees(request):
    return render(request, "employees/employees.html")

class DepartmentList(APIView):
    """
    List all departments
    """
    def get(request, format=None):
        departments = Department.objects.all()
        serializer = DepartmentSerializer(departments, many=True)
        return Response(serializer.data)


class EmployeeList(APIView):
    """
    List all employees, or add a new employees
    """
    def get(self, request, format=None):
        employees = Employee.objects.all()
        serializer = EmployeeSerializer(employees, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.error, status=status.HTTP_400_BAD_REQUEST)


class EmployeeDetail(APIView):
    """
    Get, update or delete employee
    """
    def get_object(self, pk):
        try:
            return Employee.objects.get(pk=pk)
        except Employee.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        employee = self.get_object(pk)
        serializer = EmployeeSerializer(employee)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        employee = self.get_object(pk)
        serializer = EmployeeSerializer(employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        employee = self.get_object(pk)
        employee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_status(self, status):
    """
    Get active/inactive employees
    :param status:
    :return:
    """
    employees = Employee.objects.filter(active=status.title())
    serializer = EmployeeSerializer(employees, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_employees_on_department(self, department_id):
    """
    Get employees based on department id.
    By default endpoint return only active employees.
    :param department_id:
    :return:
    """
    employees = Employee.objects.filter(department=department_id, active=True)
    serializer = EmployeeSerializer(employees, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_employees_on_status_and_department(self, department_id, status):
    """
    Get active/inactive employees based on department id
    :param self:
    :param department_id:
    :param status:
    :return:
    """
    employees = Employee.objects.filter(department=department_id, active=status.title())
    serializer = EmployeeSerializer(employees, many=True)
    return Response(serializer.data)


