from django.db import models


class Department(models.Model):
    name = models.CharField(max_length=30)


class Employee(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    active = models.BooleanField(default=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
