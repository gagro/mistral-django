from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from employees import views

urlpatterns = [
    path('', views.home, name="welcome_page"),
    path('employees', views.employees, name='employees_page'),


    path('api/departments/', views.DepartmentList.as_view()),
    # path('departments/<int:pk>'),
    path('api/employees/', views.EmployeeList.as_view()),
    path('api/employees/<int:pk>/', views.EmployeeDetail.as_view()),
    path('api/employees/<status>/', views.get_status),
    path('api/department/<int:department_id>/employees/', views.get_employees_on_department),
    path('api/department/<int:department_id>/employees/<status>/', views.get_employees_on_status_and_department)

]

urlpatterns = format_suffix_patterns(urlpatterns)