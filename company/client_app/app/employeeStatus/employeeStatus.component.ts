import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/dataService';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../shared/employee';

@Component({
  selector: 'app-format',
  templateUrl: "employeeStatus.component.html"
})
export class EmployeeStatusComponent implements OnInit {

    constructor(public data: DataService, private route: ActivatedRoute, private http: HttpClient) {

    }
    public employees: Employee[];
    public id: number;

    ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];
        this.employees = this.route.snapshot.data['employee'];
    }

}
