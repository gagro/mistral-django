﻿import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { Employee } from "./employee";
import { Department } from "./department";
import { Injectable, OnInit } from "@angular/core";

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { ActivatedRoute } from '@angular/router';
import { catchError } from "rxjs/operators/catchError";


@Injectable()
export class DataService {
    constructor(private http: HttpClient, private route: ActivatedRoute) { }

    private header: Headers;

    public loadEmployees(): Observable<Employee[]> {
        return this.http.get<Employee[]>("/api/employees")
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    public loadDepartments(): Observable<Department[]>{
        return this.http.get<Department[]>("/api/departments")
    }

    private handleError(error: HttpErrorResponse) {
        console.log(error.message);
        return Observable.throw(error.message);
    }

    public loadEmployee(id: number): Observable<Employee> {
        return this.http.get<Employee>(`/api/employees/${id}`);
    }

    public loadActiveInactiveEmployee(status: boolean): Observable<Employee> {
        return this.http.get<Employee>(`/api/employees/${status}`);
    }

    public loadEmployeesBasedOnDepartment(departmentId: number): Observable<Employee> {
        return this.http.get<Employee>(`/api/department/${departmentId}/employees`);
    }

    public loadInactiveEmployeesBasedOnDepartment(departmentId: number): Observable<Employee> {
        return this.http.get<Employee>(`/api/department/${departmentId}/employees/false`);
    }


    public addEmployee(data: Employee): Observable<Employee> {
        return this.http.post<Employee>("/api/employees/", data, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    } 

    public updateEmloyee(id: number, data: Employee): Observable<Employee> {
        return this.http.put<Employee>(`/api/employees/${id}/`, data, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }

    public deleteEmployee(id: number): Observable<Employee> {
        return this.http.delete<Employee>(`/api/employees/${id}`);
    }
}