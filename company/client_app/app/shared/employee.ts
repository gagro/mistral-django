﻿import { Department } from "./department";

export class Employee {
    first_name: string;
    last_name: string;
    email: string;
    active: boolean;
    department: number
}