import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../shared/employee';

@Component({
    templateUrl: "employee-edit.component.html"
})
export class EmployeEditComponent implements OnInit {
    @ViewChild(NgForm) employeeForm: NgForm;
    public employee: Employee;
    public departments = ['development', 'management', 'hr']

    constructor(private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.route.parent.data.subscribe(data => {
            this.employee = data['employee'];

            if (this.employeeForm) {
                this.employeeForm.reset();
            }
        })
    }

}
