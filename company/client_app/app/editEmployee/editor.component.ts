﻿import { Component, OnInit } from "@angular/core";
import { DataService } from '../shared/dataService';
import { ActivatedRoute } from '@angular/router';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Employee } from "../shared/employee";

@Component({
    selector: "editor",
    templateUrl: "editor.component.html",
    styleUrls: ['editor.component.css']
})

export class EditorComponent implements OnInit {
    constructor(public data: DataService, private router: Router, private route: ActivatedRoute, private http: HttpClient) {

    }
    public employee: Employee;
    public errorMessage = "";
    public isBusy = true;
    public id: number;

    private updateEmployee() {
        this.data.updateEmloyee(this.id, this.employee)
            .subscribe(data =>
                this.router.navigate(["/"]));
    }

    private deleteEmployee() {
        this.data.deleteEmployee(this.id)
            .subscribe(data =>
                this.router.navigate(["/"]));
    };


    ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];
        this.employee = this.route.snapshot.data['employee'];
    }
}