import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/dataService';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../shared/employee';
import { js2xml } from "xml-js";

@Component({
  selector: 'app-format',
  templateUrl: "format.component.html"
})
export class FormatComponent implements OnInit {

    constructor(public data: DataService, private route: ActivatedRoute, private http: HttpClient) {

    }
    public format: Employee;
    public id: number;
    public formatXML;

    ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];
        this.format = this.route.snapshot.data['formats'];
        this.formatXML = js2xml(this.format, { compact: true, ignoreComment: true, spaces: 4 });
    }

}
