﻿import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { DataService } from "../shared/dataService";
import { Employee } from "../shared/employee";

@Injectable()
export class FormatResolver implements Resolve<Employee> {

    constructor(private service: DataService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Employee> {
        let id = route.params['id'];
        return this.service.loadEmployee(id);
    }
}