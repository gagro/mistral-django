import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/dataService';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../shared/employee';

@Component({
  selector: 'department',
  templateUrl: "department.component.html"
})
export class DepartmentComponent implements OnInit {

    constructor(public data: DataService, private route: ActivatedRoute, private http: HttpClient) {

    }
    public employees: Employee[];
    public departmentId: number;
    public departments = ['Development', 'Management', 'HR']

    ngOnInit(): void {
        this.departmentId = this.route.snapshot.params['departmentId'];
        this.employees = this.route.snapshot.data['department'];
    }

}
