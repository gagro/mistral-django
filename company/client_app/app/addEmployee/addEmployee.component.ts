﻿import { Component, OnInit, Injectable } from "@angular/core";
import { DataService } from "../shared/dataService";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from "rxjs/Observable";
import { NgForm } from "@angular/forms";
import { Employee } from "../shared/employee";

@Component({
    selector: "add-employee",
    templateUrl: "addEmployee.component.html"
})

export class AddEmployeeComponent implements OnInit{

    constructor(private data: DataService, private route: ActivatedRoute, private router: Router) {
    }

    public employee: Employee = new Employee;
    public errorMessage = "";
    public isBusy = true;
    public deparments = {
        development: {
            id: 0,
            name: 'development'
        },
        management: {
            id: 1,
            name: 'management'
        },
        hr: {
            id: 2,
            name: 'HR'
        }
    };

    submitForm() {
        this.data.addEmployee(this.employee)
            .subscribe(data => {
                this.employee = data;
                this.router.navigate(["/"]);
            }, err => this.errorMessage = err);
    }

    ngOnInit(): void {

    }
}