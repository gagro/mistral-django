import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { DataService } from "./shared/dataService";
import { AddEmployeeComponent } from "./addEmployee/addEmployee.component";
import { EmployeeModule } from './employee/employee.module';
import { EmployeeComponent } from './employee/employee.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './page-not-found.component';
import { EmployeEditComponent } from './editEmployee/employee-edit.component';
import { FormsModule } from '@angular/forms';
import { FormatComponent } from './format/format.component';


@NgModule({
  declarations: [
      AppComponent,
      AddEmployeeComponent,
      PageNotFoundComponent,
      FormatComponent
  ],
  imports: [
      BrowserModule,
      HttpClientModule,
      EmployeeModule,
      AppRoutingModule,
      FormsModule
  ],
  providers: [
      DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
