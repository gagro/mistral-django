﻿import { Component, OnInit } from "@angular/core";
import { DataService } from "../shared/dataService";
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from "@angular/router";
import { Employee } from "../shared/employee";
import { Department } from "../shared/department";


@Component({
    selector: "employee-list",
    templateUrl: "employeeList.component.html",
    styleUrls: []
})

export class EmployeeListComponent implements OnInit {

    constructor(private data: DataService, private route: ActivatedRoute, private router: Router) {
    }
    public departments = ["development", "management", "hr"]
    public employees: Employee[];
    public filteredEmployees: Employee[];
    public active: boolean = true;
    public inactive: boolean = false;
   
    _listFilter: string;
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredEmployees = this.listFilter ? this.performFilter(this.listFilter) : this.employees;
    }
    
    public errorMessage: string;
    public isBusy = true;

    performFilter(filterBy: string): Employee[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.employees.filter((employee: Employee) =>
            employee.first_name.toLocaleLowerCase().indexOf(filterBy) !== -1);
    }

    private deleteEmployee(id: number) {
        this.data.deleteEmployee(id)
            .subscribe(data => {
                this.ngOnInit();
    }
    )};

    ngOnInit(): void {
        console.log("type of ", typeof(this.departments))
        this.data.loadEmployees()
            .subscribe(employees => {
                this.employees = employees;
                this.filteredEmployees = this.employees;
                this.filteredEmployees = this.filteredEmployees.reverse();
                this.listFilter = this.route.snapshot.queryParams['filterBy'] || '';
                this.isBusy = false;
            },
            error => this.errorMessage = <any>error);

        // TO DO
        this.route.data.map(data => data.employeeList).subscribe(employees => {
            this.employees = employees;
            this.filteredEmployees = this.employees
            this.filteredEmployees = this.filteredEmployees.reverse();
            this.listFilter = this.route.snapshot.queryParams['filterBy'] || '';
            this.isBusy = false;
        });
    }
}