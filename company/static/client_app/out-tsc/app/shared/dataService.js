var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { ActivatedRoute } from '@angular/router';
var DataService = /** @class */ (function () {
    function DataService(http, route) {
        this.http = http;
        this.route = route;
    }
    DataService.prototype.loadEmployees = function () {
        return this.http.get("/api/employees")
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    DataService.prototype.handleError = function (error) {
        console.log(error.message);
        return Observable.throw(error.message);
    };
    DataService.prototype.loadEmployee = function (id) {
        return this.http.get("/api/employees/" + id);
    };
    DataService.prototype.loadActiveInactiveEmployee = function (status) {
        return this.http.get("/api/employees/" + status);
    };
    DataService.prototype.loadEmployeesBasedOnDepartment = function (departmentId) {
        return this.http.get("/api/department/" + departmentId + "/employees");
    };
    DataService.prototype.loadInactiveEmployeesBasedOnDepartment = function (departmentId) {
        return this.http.get("/api/department/" + departmentId + "/employees/false");
    };
    DataService.prototype.addEmployee = function (data) {
        return this.http.post("/api/employees", data, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    };
    DataService.prototype.updateEmloyee = function (id, data) {
        return this.http.put("/api/employees/" + id, data, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    };
    DataService.prototype.deleteEmployee = function (id) {
        return this.http.delete("/api/employees/" + id);
    };
    DataService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, ActivatedRoute])
    ], DataService);
    return DataService;
}());
export { DataService };
//# sourceMappingURL=dataService.js.map