var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
var EmployeEditComponent = /** @class */ (function () {
    function EmployeEditComponent(route) {
        this.route = route;
        this.departments = {
            development: {
                id: 0,
                name: 'development'
            },
            management: {
                id: 1,
                name: 'management'
            },
            hr: {
                id: 2,
                name: 'HR'
            }
        };
    }
    EmployeEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.parent.data.subscribe(function (data) {
            _this.employee = data['employee'];
            _this.employee.department = _this.departments[_this.employee.department.name.toLowerCase()];
            if (_this.employeeForm) {
                _this.employeeForm.reset();
            }
        });
    };
    __decorate([
        ViewChild(NgForm),
        __metadata("design:type", NgForm)
    ], EmployeEditComponent.prototype, "employeeForm", void 0);
    EmployeEditComponent = __decorate([
        Component({
            templateUrl: "employee-edit.component.html"
        }),
        __metadata("design:paramtypes", [ActivatedRoute])
    ], EmployeEditComponent);
    return EmployeEditComponent;
}());
export { EmployeEditComponent };
//# sourceMappingURL=employee-edit.component.js.map