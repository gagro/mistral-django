var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { DataService } from '../shared/dataService';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
var EditorComponent = /** @class */ (function () {
    function EditorComponent(data, router, route, http) {
        this.data = data;
        this.router = router;
        this.route = route;
        this.http = http;
        this.errorMessage = "";
        this.isBusy = true;
    }
    EditorComponent.prototype.updateEmployee = function () {
        var _this = this;
        this.data.updateEmloyee(this.id, this.employee)
            .subscribe(function (data) {
            return _this.router.navigate(["/"]);
        });
    };
    EditorComponent.prototype.deleteEmployee = function () {
        var _this = this;
        this.data.deleteEmployee(this.id)
            .subscribe(function (data) {
            return _this.router.navigate(["/"]);
        });
    };
    ;
    EditorComponent.prototype.ngOnInit = function () {
        this.id = this.route.snapshot.params['id'];
        this.employee = this.route.snapshot.data['employee'];
    };
    EditorComponent = __decorate([
        Component({
            selector: "editor",
            templateUrl: "editor.component.html",
            styleUrls: ['editor.component.css']
        }),
        __metadata("design:paramtypes", [DataService, Router, ActivatedRoute, HttpClient])
    ], EditorComponent);
    return EditorComponent;
}());
export { EditorComponent };
//# sourceMappingURL=editor.component.js.map