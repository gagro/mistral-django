var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { DataService } from "../shared/dataService";
import { ActivatedRoute, Router } from "@angular/router";
var EmployeeListComponent = /** @class */ (function () {
    function EmployeeListComponent(data, route, router) {
        this.data = data;
        this.route = route;
        this.router = router;
        this.active = true;
        this.inactive = false;
        this.isBusy = true;
    }
    Object.defineProperty(EmployeeListComponent.prototype, "listFilter", {
        get: function () {
            return this._listFilter;
        },
        set: function (value) {
            this._listFilter = value;
            this.filteredEmployees = this.listFilter ? this.performFilter(this.listFilter) : this.employees;
        },
        enumerable: true,
        configurable: true
    });
    EmployeeListComponent.prototype.performFilter = function (filterBy) {
        filterBy = filterBy.toLocaleLowerCase();
        return this.employees.filter(function (employee) {
            return employee.firstName.toLocaleLowerCase().indexOf(filterBy) !== -1;
        });
    };
    EmployeeListComponent.prototype.deleteEmployee = function (id) {
        var _this = this;
        this.data.deleteEmployee(id)
            .subscribe(function (data) {
            _this.ngOnInit();
        });
    };
    ;
    EmployeeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.loadEmployees()
            .subscribe(function (employees) {
            _this.employees = employees;
            _this.filteredEmployees = _this.employees;
            _this.filteredEmployees = _this.filteredEmployees.reverse();
            _this.listFilter = _this.route.snapshot.queryParams['filterBy'] || '';
            _this.isBusy = false;
        }, function (error) { return _this.errorMessage = error; });
        // TO DO
        this.route.data.map(function (data) { return data.employeeList; }).subscribe(function (employees) {
            _this.employees = employees;
            _this.filteredEmployees = _this.employees;
            _this.filteredEmployees = _this.filteredEmployees.reverse();
            _this.listFilter = _this.route.snapshot.queryParams['filterBy'] || '';
            _this.isBusy = false;
        });
    };
    EmployeeListComponent = __decorate([
        Component({
            selector: "employee-list",
            templateUrl: "employeeList.component.html",
            styleUrls: []
        }),
        __metadata("design:paramtypes", [DataService, ActivatedRoute, Router])
    ], EmployeeListComponent);
    return EmployeeListComponent;
}());
export { EmployeeListComponent };
//# sourceMappingURL=employeeList.component.js.map