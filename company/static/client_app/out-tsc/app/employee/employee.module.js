var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeListComponent } from "../employee/employeeList.component";
import { EmployeeComponent } from "./employee.component";
import { EditorComponent } from "../editEmployee/editor.component";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AddEmployeeComponent } from '../addEmployee/addEmployee.component';
import { EditorResolver } from '../editEmployee/editor-resolver.service';
import { EmployeEditComponent } from '../editEmployee/employee-edit.component';
import { FormatComponent } from '../format/format.component';
import { FormatResolver } from '../format/format-resolver.service';
import { EmployeeListResolver } from './employeeList-resolver.service';
import { DepartmentComponent } from '../department/department.component';
import { DepartmentResolver } from '../department/department-resolver.service';
import { EmployeeStatusComponent } from '../employeeStatus/employeeStatus.component';
import { EmployeeStatusResolver } from '../employeeStatus/employeeStatus-resolver.service';
var EmployeeModule = /** @class */ (function () {
    function EmployeeModule() {
    }
    EmployeeModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                RouterModule.forChild([
                    { path: '', component: EmployeeComponent, resolve: { employeeList: EmployeeListResolver }, pathMatch: 'full' },
                    {
                        path: ":id/edit",
                        component: EditorComponent,
                        resolve: { employee: EditorResolver },
                        children: [
                            {
                                path: '', redirectTo: 'info', pathMatch: 'full'
                            },
                            {
                                path: 'info', component: EmployeEditComponent
                            },
                        ]
                    },
                    { path: 'addEmployee', component: AddEmployeeComponent },
                    { path: ':id/format', component: FormatComponent, resolve: { formats: FormatResolver } },
                    { path: 'department/:departmentId/employees', component: DepartmentComponent, resolve: { department: DepartmentResolver } },
                    { path: 'employees/:active', component: EmployeeStatusComponent, resolve: { employee: EmployeeStatusResolver } },
                ])
            ],
            declarations: [
                EmployeeListComponent,
                EmployeeComponent,
                EditorComponent,
                EmployeEditComponent,
                DepartmentComponent,
                EmployeeStatusComponent
            ],
            providers: [
                EditorResolver,
                FormatResolver,
                EmployeeListResolver,
                DepartmentResolver,
                EmployeeStatusResolver
            ]
        })
    ], EmployeeModule);
    return EmployeeModule;
}());
export { EmployeeModule };
//# sourceMappingURL=employee.module.js.map