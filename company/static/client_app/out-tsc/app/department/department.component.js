var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../shared/dataService';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
var DepartmentComponent = /** @class */ (function () {
    function DepartmentComponent(data, route, http) {
        this.data = data;
        this.route = route;
        this.http = http;
    }
    DepartmentComponent.prototype.ngOnInit = function () {
        this.departmentId = this.route.snapshot.params['departmentId'];
        this.employees = this.route.snapshot.data['department'];
    };
    DepartmentComponent = __decorate([
        Component({
            selector: 'department',
            templateUrl: "department.component.html"
        }),
        __metadata("design:paramtypes", [DataService, ActivatedRoute, HttpClient])
    ], DepartmentComponent);
    return DepartmentComponent;
}());
export { DepartmentComponent };
//# sourceMappingURL=department.component.js.map