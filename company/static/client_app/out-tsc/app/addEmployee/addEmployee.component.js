var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { DataService } from "../shared/dataService";
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from "../shared/employee";
var AddEmployeeComponent = /** @class */ (function () {
    function AddEmployeeComponent(data, route, router) {
        this.data = data;
        this.route = route;
        this.router = router;
        this.employee = new Employee;
        this.errorMessage = "";
        this.isBusy = true;
        this.deparments = {
            development: {
                id: 0,
                name: 'development'
            },
            management: {
                id: 1,
                name: 'management'
            },
            hr: {
                id: 2,
                name: 'HR'
            }
        };
    }
    AddEmployeeComponent.prototype.submitForm = function () {
        var _this = this;
        this.data.addEmployee(this.employee)
            .subscribe(function (data) {
            _this.employee = data;
            _this.router.navigate(["/"]);
        }, function (err) { return _this.errorMessage = err; });
    };
    AddEmployeeComponent.prototype.ngOnInit = function () {
    };
    AddEmployeeComponent = __decorate([
        Component({
            selector: "add-employee",
            templateUrl: "addEmployee.component.html"
        }),
        __metadata("design:paramtypes", [DataService, ActivatedRoute, Router])
    ], AddEmployeeComponent);
    return AddEmployeeComponent;
}());
export { AddEmployeeComponent };
//# sourceMappingURL=addEmployee.component.js.map